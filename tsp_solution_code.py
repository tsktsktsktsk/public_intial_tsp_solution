# -*- coding: utf-8 -*-
"""
Created on Sun Feb 26 15:39:10 2017

@author: ik-be
"""

import pandas as pd
from copy import deepcopy #Might be a slowdown
import numpy as np
import time
from matplotlib import pyplot as plt


##Possible slowdowns:
# - Deepcopy
# - appending
# - multiple function calls

class array_builder:
    
    def __init__(self, excel_file):
        self.excel_file = excel_file
        self.sheetnames = excel_file.sheet_names
        self.consignments = excel_file.parse(self.sheetnames[-2])
        self.depot = excel_file.parse('Depot').iloc[0]
        self.time_xldf = excel_file.parse(self.sheetnames[1])
        self.dist_xldf = excel_file.parse(self.sheetnames[0])
     
        
    def time_df_builder(self):
        self.time_df = pd.DataFrame(self.time_xldf.iloc[1:, 1:])
        self.time_df.index = self.time_xldf.iloc[0][1:]
        self.time_df.columns = self.time_xldf.iloc[0][1:]  
        return self.time_df
    
    def dist_df_builder(self):        
        self.dist_df = pd.DataFrame(self.dist_xldf.iloc[1:, 1:])
        self.dist_df.index = self.dist_xldf.iloc[0][1:]
        self.dist_df.columns = self.dist_xldf.iloc[0][1:]
        return self.dist_df
    
    def consignment_time_rewriter(self):
        lower_bound, upper_bound = [], []
        for i in xrange(len(self.consignments)):
            lower_bound.append(self.time_min(self.consignments.loc[i][-4]))
            upper_bound.append(self.time_min(self.consignments.loc[i][-3]))
            
        self.consignments['Begin aankomsttijd'] = lower_bound
        self.consignments['Eind aankomsttijd'] = upper_bound
        return self.consignments 
    
    def db_builder(self, key):
        hist = {}
        group = {}
        
        for i in xrange(len(self.consignments)):
            if key == 'Postcode':
                df = self.consignments[key]
                pc = df[i][:4]
                try:
                    hist[str(pc)] += 1
                    group[str(pc)].append(self.consignments.loc[i]) 
                except KeyError:
                    hist[str(pc)] = 1
                    group[str(pc)] = [self.consignments.loc[i]]
            elif key == 'Adres':
                df = self.consignments[key]
                adress = df[i]
                try:
                    hist[adress] += 1
                    group[adress].append(self.consignments.loc[i]) 
                except KeyError:
                    hist[adress] = 1
                    group[adress] = [self.consignmentsiloc[i]]
                    
            elif key == 'layer':
                pc = self.consignments['Postcode'][i][:4]
                adress = self.consignments['Adres'][i]
                try:
                    if len(group[pc]) == 0:
                        group[pc] = {}
                except KeyError:
                    group[pc] = {}
                try:
                    group[pc][adress].append(self.consignments.loc[i])
                except KeyError:
                    
                    group[pc][adress] = [self.consignments.loc[i]] 
        return group, hist  
        
    def cost_array_builder(self):
        per_m   = 0.0002
        per_min = 0.25
        self.time_df_builder()
        self.dist_df_builder()
        
        #calc cost_df
        self.time_cost_df = self.time_df * per_min
        self.dist_cost_df = self.dist_df * per_m
        self.cost_df = self.time_cost_df + self.dist_cost_df 
        return [self.time_cost_df, self.dist_cost_df, self.cost_df]
        
    def pc_hist_group(self):
        self.pc_group, self.pc_hist = self.db_builder('Postcode')
        return [self.pc_group, self.pc_hist], deepcopy([self.pc_group, self.pc_hist])
    
    def straat_hist_group(self):
        self.straat_group, self.straat_hist = self.db_builder('Adres')
        return [self.straat_group, self.straat_hist], deepcopy([self.straat_group, self.straat_hist])
    
    def pc_straat_groups(self):
        self.pc_straat_group, self.empty_hist = self.db_builder('layer')
        return [self.pc_straat_group, self.empty_hist], deepcopy([self.pc_straat_group, self.empty_hist])
    
    def time_min(self, time_tuple):
        #converts the time to a minute integer (Helper function)
        t = str(time_tuple)
        return int(t[:2])*60 + int(t[3:5])     
        
                 
start = time.time()
print "START", time.time() - start
data_path = '/home/casper/Repos/testfolder/'
case_file = 'Data_TNTCase (2016) (002).xlsx'
solution_file = 'SolutionTemplate_TNTCase (2016) (002).xlsx'
excel_file = pd.ExcelFile(data_path + case_file)
print "LOADED EXCEL FILE", time.time() - start

a = array_builder(excel_file)
print "ARRAYS INIT", time.time() - start
available_consigs = a.consignment_time_rewriter()  
street_pc_dict = a.pc_straat_groups()[1][0]
cost_array = a.cost_array_builder()[2]
time_array = a.time_df_builder()
group, hist = a.db_builder('layer')

depot = a.depot
print "ARRAYS BUILD - DONE", time.time() - start


class Checks:
    
    def __init__(self, vanObj, time_array, next_consignment = None):
        self.vanObj       = vanObj
        self.cur_consig   = vanObj.route_order[-1]
        self.next_consig  = next_consignment
        self.cur_weight   = vanObj.current_weight
        self.cur_time     = vanObj.current_time
        self.time_ar      = time_array
        self.max_time     = 1095 #Time to be back at the depot
        self.max_weight   = 1100
        
    def time_check(self):
        lower_bound = self.next_consig['Begin aankomsttijd']
        upper_bound = self.next_consig['Eind aankomsttijd']
        self.go_depot()
        arrival_time = self.cur_time + self.travel_time()
        #print "NAME:" ,self.next_consig.name, "PC", self.next_consig['Postcode'], "TIMES: ",lower_bound, self.cur_time, self.travel_time(), upper_bound, (lower_bound <= self.cur_time + self.travel_time() <= upper_bound)
        if (25 + arrival_time + self.depot_travel_time) - self.vanObj.start_time <= self.vanObj.max_time: #Check max hours not exceeding
            if lower_bound <= arrival_time <= upper_bound: #If the package is deliverd in the right time interval
                if 20 + arrival_time + self.depot_travel_time <= self.max_time:
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False
    
    def weight_check(self):       
        if self.next_consig[0] == 'C':
            if self.cur_weight - self.next_consig['Gewicht'] <= self.max_weight:
                return True
            else:
                return False
        else:
            if self.cur_weight + self.next_consig['Gewicht'] <= self.max_weight:
                return True
            else:
                return False

    def waiting_time(self):
        arrival_time = self.cur_time + self.travel_time()
        lower_bound = self.next_consig['Begin aankomsttijd']
        return lower_bound - arrival_time
        
    #Returns an integer with the travel_time
    def travel_time(self):
        current_pc     = int(self.cur_consig['Postcode'][:4])
        current_adress = self.cur_consig['Adres']
        next_pc        = int(self.next_consig['Postcode'][:4])
        next_adress    = self.next_consig['Adres']
        
        if current_pc != next_pc:
            travel_time = self.time_ar[current_pc][next_pc]
        elif current_pc == next_pc and current_adress != next_adress:
            travel_time = 2
        else:
            travel_time = 0
        return travel_time
            
    def is_option(self):
        if self.time_check() == True and self.weight_check() == True:
            return [True, self.travel_time()]
        else:
            return [False]
        
    def go_depot(self):
        current_pc = int(self.cur_consig['Postcode'][:4])
        self.depot_travel_time = self.time_ar[current_pc]['DEPOT']
        if self.vanObj.current_time + self.depot_travel_time >= self.max_time:
            return [True, self.depot_travel_time]
        else:
            return [False, self.depot_travel_time]

class Van:
    
    def __init__(self, depot):        
        self.start_time     = 390
        self.stop_time      = 1095 #Has to be back at the depot by this time.
        
        self.day_cost       = 20

        self.pickup_time    = 5
        self.drop_time      = 4
        self.max_time       = 600
        
        self.load_time      = 30
        self.dropping_time  = 15
        
        self.route_order    = [depot]
        self.arrival_times  = []
        self.weights        = []
        self.costs          = []
        self.consig_nrs     = []
        self.order_travel_times = []
        self.order_types = []
        
        self.current_time   = self.start_time 
        self.current_weight = 0
        self.current_cost   = 0


        
    def update_stats(self, order):
        consig = order[0]
        order_travel_time = order[1]
        
        order_type = consig[0]
        order_weight = consig[-2]    
        
        self.route_order.append(consig)
        self.arrival_times.append(self.current_time)
        self.weights.append(order_weight)
        self.order_types.append(consig[0])
        self.consig_nrs.append(consig.name)
        self.order_travel_times.append(order_travel_time)
        
        self.update_weight(order_type, order_weight)
        self.update_time_regular(order_travel_time)
        
        #print "CONSIG STATS:", consig.name, " PC: ", order_pc, " ADRESS: ", order_street_name, "TRAVEL TIME: ", order_travel_time, " TYPE: ", order_type, "WEIGHT", order_weight
        #print "CUR TIME: ", self.current_time, " LOWERBOUND TIME: ", consig.iloc[8], " UPPERBOUND TIME: ",consig.iloc[9],  "CUR WEIGHT", self.current_weight, "CUR COST: ", self.current_cost, "\n"
    
    def update_weight(self, order_type, order_weight):
        if order_type == 'C':
            self.current_weight -= order_weight
        else:
            self.current_weight += order_weight
            
    def check_same_adress(self, current_consig, previous_consig):
        if current_consig[2] == previous_consig[2]:
            return True
        else:
            return False   
            
    def update_time_regular(self, order_travel_time): #Correct time int check with delivery + pick up times (dependent)
        self.current_time += order_travel_time
        current_order = self.route_order[-1]
        if len(self.route_order) > 1:
            previous_order = self.route_order[-2]
            if current_order[2] == previous_order[2]:
                if self.time_corrected == False:
                    if current_order[0] == 'C' and previous_order[0] == 'D': 
                        self.time_corrected == True
                        self.current_time += 1 #the difference between pickup (5) and drop (4) is 1 so that has to be added.
            else:
                if current_order[0] == 'C':
                    self.current_time += self.pickup_time
                    self.time_corrected = False
                else:
                    self.current_time += self.drop_time
                    self.time_corrected = False
                
                
        else:
            if current_order[0] == 'C':
                self.current_time += self.pickup_time
                self.time_corrected = False
            else:
                self.current_time += self.drop_time
                self.time_corrected = False
                
    def update_going_home(self, depot, travel_time):
        self.current_time += travel_time
        self.current_time += self.dropping_time
        self.route_order.append(depot)
        self.arrival_times.append(self.arrival_times[-1] + travel_time)

        
class Planner:
    
    def __init__(self, consignments, depot, time_array, cost_array, layered_db):
        self.van_list = []
        self.available_consignments = consignments
        self.depot = depot
        self.time_array = time_array
        self.cost_array = cost_array
        self.layered_db = layered_db
        
    def drive_all_consigs(self):
        iterator = 0
        while len(self.available_consignments) > 0:
            if self.drive_van() == False:
                break
            print iterator, len(self.available_consignments)
            iterator += 1
        return self.van_list, self.available_consignments
    
    def drive_van(self):
        if self.start_tour() != False:
            next_consig = self.do_next_consig()
            while next_consig is not None and Checks(self.van_list[-1], self.time_array).go_depot()[0] == False:
                next_consig = self.do_next_consig()
            self.go_home(Checks(self.van_list[-1], self.time_array).go_depot()[1])
            return True
        else:
            return False
        
    def start_tour(self):
        self.van_list.append(deepcopy(Van(depot)))
        start_selector_obj = Selector(self.van_list[-1], self.available_consignments, 
                                      self.time_array, self.cost_array, self.layered_db)
        start_consig = start_selector_obj.select_start_consig()
        if start_consig is not None:
            self.update_all_stats(start_consig)
            return True
        else:
            for i in range(390, 1095, 5):
                self.van_list[-1].start_time = i
                self.van_list[-1].current_time = i
                start_selector_obj = Selector(self.van_list[-1], self.available_consignments, 
                                              self.time_array, self.cost_array, self.layered_db)
                start_consig = start_selector_obj.select_start_consig()
                if start_consig is not None:
                    print i
                    self.update_all_stats(start_consig)
                    return True
                
            return False


    def do_next_consig(self):
        start_selector_obj = Selector(self.van_list[-1], self.available_consignments, 
                                      self.time_array, self.cost_array, self.layered_db)
        next_consig = start_selector_obj.select_next_stop()
        if next_consig is not None:
            self.update_all_stats(next_consig)
        return next_consig
    
    def go_home(self, travel_time):
        self.van_list[-1].update_going_home(self.depot, travel_time)
        
    def update_all_stats(self, consignment):
        self.update_current_consig_dbs(consignment[0])
        self.van_list[-1].update_stats(consignment)
        
        
    def update_current_consig_dbs(self, consignment):
        #If the van and the assigned consignment pass the checks the DBs get updated
        self.available_consignments = self.available_consignments.drop(consignment.name)
        same_street = self.layered_db[consignment['Postcode'][:4]][consignment['Adres']]

        for i in range(len(same_street)):
            if same_street[i].name == consignment.name:
                #print "UPDATE DB BEFORE: ",i, self.layered_db[str(order_pc)][order_street_name] 
                self.layered_db[consignment['Postcode'][:4]][consignment['Adres']].pop(i)
                #print "UPDATE DB AFTER: ", i, str(order_pc), order_street_name, self.layered_db[str(order_pc)][order_street_name]
                break

class Selector:
    #Makes suggestions to the check class and if it passes it will select it as the next stop
    def __init__(self, vanObj, available_consignments, time_array, cost_array, layered_db):
        self.vanObj            = vanObj
        self.cur_consig        = vanObj.route_order[-1]
        self.current_pc        = self.cur_consig['Postcode'][:4]
        self.current_street    = self.cur_consig['Adres']
        self.cur_weight        = vanObj.current_weight
        self.cur_time          = vanObj.current_time
        self.available_consigs = available_consignments
        self.time_array        = time_array
        self.cost_array        = cost_array
        self.layered_db        = layered_db
        
    def select_next_stop(self):
        #Starts looking for similar streets and redirects them to the check class
        #If that is exhausted it will go looking for close-by postcodes
        #Keeps looking till all postcodes are empty.
        #This is the 'algorithm'
        same_adress = self.check_same_adress()
        if same_adress is not None:
            #print "Same Adress"
            return same_adress
        
        same_pc = self.check_same_pc()
        if same_pc is not None:
            #print "Same PC"
            return same_pc
        
        cost_list = self.cost_array[int(self.current_pc)]
        cost_sorted_pcs = cost_list.order().index
        for pc in cost_sorted_pcs:
            if pc != 'DEPOT':
                for adress in self.layered_db[str(pc)].keys():
                    for consig in self.layered_db[str(pc)][adress]:
                        consig_check = Checks(self.vanObj, self.time_array, consig).is_option()
                        if consig_check[0] == True:
                            #print "OTHER"
                            return [consig, consig_check[1]]

    def check_same_adress(self):
        same_adresses = self.layered_db[self.current_pc][self.current_street]
        if len(same_adresses) > 1:
            for consig in same_adresses:
                consig_check = Checks(self.vanObj, self.time_array, consig).is_option()
                if consig_check[0] == True:
                    return [consig, consig_check[1]]
                
    def check_same_pc(self):
        adresses = self.layered_db[self.current_pc].keys()
        for adress in adresses:
            for consig in self.layered_db[self.current_pc][adress]:
                consig_check = Checks(self.vanObj, self.time_array, consig).is_option()
                if consig_check[0] == True:
                    return [consig, consig_check[1]]
        
    def select_start_consig(self):
        cost_list = self.cost_array[int(self.current_pc)]
        cost_sorted_pcs = cost_list.order().index 
        for pc in cost_sorted_pcs[2:]:
            adresses = self.layered_db[str(pc)].keys()
            for adress in adresses:
                for consig in self.layered_db[str(pc)][adress]:
                    consig_check = Checks(self.vanObj, self.time_array, consig).is_option()
                    if consig_check[0] == True:
                        return [consig, consig_check[1]]

        
#The solution
van_list, consigs = Planner(available_consigs, depot, time_array, cost_array, group).drive_all_consigs()


### Build solution template
dist_array = a.dist_df_builder()


def calc_route_dist(route_list, dist_array):
    dist_travelled = 0
    for i in range(len(route_list)-1):
        cur_pc = int(route_list[i]['Postcode'][:4])
        next_pc = int(route_list[i+1]['Postcode'][:4])
        dist_travelled += dist_array[cur_pc][next_pc]

    return dist_travelled
per_m = 0.0002
per_min = 0.25

### Eindoplossing wagens
wagen_nrs = []
aantal_orders = []
totale_kosten = []
totale_uren = []
totale_kms = []
for i in range(len(van_list)):
    routes = van_list[i].route_order
    Totale_kosten_per_wagen = 20 + calc_route_dist(routes, dist_array) * per_m +(van_list[i].current_time - van_list[i].start_time) * per_min
    Totale_werkuren = van_list[i].current_time - van_list[i].start_time
    Totale_kilometers = calc_route_dist(routes, dist_array) * 0.001
    
    wagen_nrs.append(i)
    aantal_orders.append(len(routes) - 2)
    totale_kosten.append(Totale_kosten_per_wagen)
    totale_uren.append(Totale_werkuren/ 60.0)
    totale_kms.append(Totale_kilometers)
                         
Eindoplossing_wagens = {'Wagennummer': wagen_nrs, 
                              'Aantal orders': aantal_orders,
                              'Totale kosten per wagen': totale_kosten,
                              'Aantal werkuren': totale_uren,
                              'Aantal km': totale_kms}

eindoplossing_wagens = pd.DataFrame(Eindoplossing_wagens)


#### Eindoplossing orders
eind_oplossing = []
for i in range(len(van_list)):
    route = van_list[i].route_order
    wagen_nr = i
    for j in range(1, len(route[1:])):
        order_nr = route[j]['OrderNr']
        stopvolgnr = j
        eind_oplossing.append([str(order_nr), wagen_nr, stopvolgnr])
eind_oplossing
titles = ['Ordernummer', 'Wagennummer', 'Stopvolgnummer']
eind_oplossing_orders = pd.DataFrame(eind_oplossing)
eind_oplossing_orders.columns = titles


from pandas import ExcelWriter
writer = ExcelWriter('/home/casper/Repos/testfolder/solution_template_final.xls')
eind_oplossing_orders.to_excel(writer, 'Eind oplossing Orders')
eindoplossing_wagens.to_excel(writer, 'Eind oplossing Wagens')
writer.save()

## Plot stuff


class centroid_plot:
    
    def pc_hist_coords(self, consigs):
        pc_dict = {}
        for i in range(len(consigs)):
            try:
                pc_dict[consigs.iloc[i][3][:4]].append([consigs.iloc[i][4], consigs.iloc[i][5]])
            except KeyError:
                pc_dict[consigs.iloc[i][3][:4]] = [[consigs.iloc[i][4], consigs.iloc[i][5]]]
        return pc_dict
        
    def pc_centroids(self, pc_dict):
        cents = np.zeros((len(pc_dict), 4))
        counter = 0
        for key in pc_dict.keys():
            x_coords, y_coords = [], []
            for i in range(len(pc_dict[key])):
                x_coords.append(pc_dict[key][i][0])
                y_coords.append(pc_dict[key][i][1])
            cents[counter, 0] = len(pc_dict[key])
            cents[counter, 1] = np.mean(x_coords)
            cents[counter, 2] = np.mean(y_coords)
            cents[counter, 3] = key
            counter += 1
        return cents

def scatter_pc_centroids(centroids, depot_x, depot_y, color): 
    plt.scatter(centroids[:,1], centroids[:,2], s=centroids[:,0]*16, c = color)
    plt.scatter(depot_x, depot_y, s= 100, c = 'k')

def plot_depot(consig, colour):
    pt1 = [6.0051154, 51.9705842]
    pt2 = [consig['X coordinaat'], consig['Y coordinaat']]
    plt.plot([pt1[0], pt2[0]], [pt1[1], pt2[1]], color=colour, linestyle='-', linewidth=2)  
    
def plot_line(consig_1, consig_2, colour):
    pt1 = [consig_1['X coordinaat'], consig_1['Y coordinaat']]
    pt2 = [consig_2['X coordinaat'], consig_2['Y coordinaat']]
    plt.plot([pt1[0], pt2[0]], [pt1[1], pt2[1]], color=colour, linestyle='-', linewidth=2)

def plot_route(routes, route_color):
    plot_depot(routes[1], colour[route_color]) 
    for i in range(1, len(routes[1:-1])):
        plot_line(routes[i], routes[i+1], colour[route_color])  
    plot_depot(routes[-2], colour[route_color])        

def adjust_consigs(consigs, route):
    for consig in route[1:-1]:
        consigs = consigs.drop(consig.name)
    return consigs



route_color = 3
total_color = 0
colour = ['r', 'm', 'y', 'g', 'b']
depot_x, depot_y = 6.0051154, 51.9705842
new_consigs = available_consigs

plt.figure(figsize = (10,8))
consig_cent_hist = centroid_plot().pc_hist_coords(new_consigs)
consig_centroids = centroid_plot().pc_centroids(consig_cent_hist)
scatter_pc_centroids(consig_centroids, depot_x, depot_y, colour[3])

plt.show()
import matplotlib.patches as mpatches

for i in range(len(van_list)):        
    if i % 5 == 0:
        plt.figure(figsize = (10,7))
        consig_cent_hist = centroid_plot().pc_hist_coords(new_consigs)
        consig_centroids = centroid_plot().pc_centroids(consig_cent_hist)
        scatter_pc_centroids(consig_centroids, depot_x, depot_y, colour[total_color])
        legend_list = []
        
    routes = van_list[i].route_order
    new_consigs = adjust_consigs(new_consigs, routes)
    routes_cent_hist = centroid_plot().pc_hist_coords(pd.DataFrame(routes[1:-1]))
    routes_centroids = centroid_plot().pc_centroids(routes_cent_hist)
    scatter_pc_centroids(routes_centroids, depot_x, depot_y, colour[i%5])
    plot_route(routes, i%5)
    red_patch = mpatches.Patch(color=colour[i%5], label='van number ' + str(i))
    legend_list.append(red_patch)
    plt.legend(handles=legend_list)


plt.show()    


